FROM golang:1.17 as builder

ARG HUGO_VERSION=v0.79.0
RUN git clone https://github.com/gohugoio/hugo.git /go/src/hugo

WORKDIR /go/src/hugo
RUN git checkout ${HUGO_VERSION}
RUN CGO_ENABLED=1 go build --tags extended

FROM ubuntu:20.04

COPY --from=builder /go/src/hugo /usr/local/bin

ENTRYPOINT ["/usr/local/bin/hugo"]
