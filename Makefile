
VERSION                 ?= $(shell git describe --tags --always --dirty)
RELEASE_VERSION         ?= $(shell git describe --abbrev=0)
DOCKER_REPO							?= dereulenspiegel
DOCKER_IMAGE            ?= $(DOCKER_REPO)/hugo

.PHONY: docker/push

docker/push:
	docker buildx build \
		--push \
		--build-arg HUGO_VERSION=v0.92.0 \
		--platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
		--tag $(DOCKER_IMAGE):$(RELEASE_VERSION) \
		--tag $(DOCKER_IMAGE):latest .
